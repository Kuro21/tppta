console.log("Hello World");
//alert("Passou aqui - chamou a fun~]ao gravar");

let nome = "Gobbato";
let idade = 40;
let isProfessor = true;

console.log(typeof(nome));
console.log(typeof(idade));
console.log(typeof(isProfessor));
console.log("2"+isProfessor);

console.log("A soma de 2 + 2 é " + (2+2));
console.log(`A soma de 2 + 2 é ${2+2}`);

//console.log("Alguma coisa");
//alert("alguma coisa");

function escreverNoConsole(){
    console.log("Olá");
}

escreverNoConsole();

function podeDirigir(idade, cnh){
    if(idade>=18 && cnh == true){
        console.log("Pode Dirigir");
    }else{
        console.log("Não Pode Dirigir");
    }    
}

podeDirigir(40,true);

const parOuImpar = (valor) => {
    return valor % 2;    
}

console.log(parOuImpar(99));

let n = 5;

if(parOuImpar(n)==0){
    console.log("É par!");
    console.log(`O número ${n} é PAR`);
}else{
    console.log("É ímpar");
    console.log(`O número ${n} é ÍMPAR`);
}

let numeros = [1,3,5,8,12];

console.log(numeros);
console.log(numeros.length);

console.log(numeros[0]);

let peixes = ['acara bandeira', 'palhaço', 'mandarim', 'esturjão'];
remover_ultimo = peixes.pop();
adicionar = peixes.push('peixe espada');

console.log(peixes);

const aluno = {
    nome:"alexandre",
    sobrenome:"Gobbato"
}

let aluno = {
    nome:"",
    setNome: function(vNome){
        this.nome = vNome;
    },
    getNome: function(){
        return this.nome;
    }
}

class Pessoa{
    constructor(nome){
        this.nome = nome;
    }

    get getNome(){
        return this.nome;
    }

    set setNome(value){
        this.nome = value;
    } 
}